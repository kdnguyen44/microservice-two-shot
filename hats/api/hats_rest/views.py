from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json

from .models import Hats, LocationVO


# Create your views here.
class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href", "id"]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "style_name",
        "fabric",
        "color",
        "picture",
        "location",
        "id",
    ]

    encoders= {
        "location": LocationVOEncoder(),
    }

class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "style_name",
        "fabric",
        "color",
        "picture",
        "location",
        "id",
    ]

    encoders= {
        "location": LocationVOEncoder(),
    }

require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            location_id = content["location"]
            location = LocationVO.objects.get(id=location_id)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,
        )

require_http_methods(["GET", "DELETE"])
def api_hat_details(request, pk):
    if request.method == "GET":
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatsDetailEncoder,
            safe=False,
        )
    else:
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
