from django.db import models

# Create your models here.
class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)

    def __str__(self):
        return self.closet_name

class Hats(models.Model):
    fabric = models.CharField(max_length=200, null=True)
    style_name = models.CharField(max_length=200, null=True)
    color = models.CharField(max_length=200, null=True)
    picture = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hat",
        on_delete=models.CASCADE,
    )
