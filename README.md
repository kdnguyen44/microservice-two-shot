# Wardrobify

Team:

* Kevin Nguyen - Hats
* Richard Kwong - Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.
I created each individual models using the fearless-front-end project as a reference. Fear-front-end helped me integrating the models to wardrobe microservices.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

I created two models in my hats microservice. One model for the hats information and one model for the Location Value Object. We use a poller to poll the data from the warddrobe to get the location for the hats.
After creating the models, we would create API views that would show us the list of hats. We use JSX in react to render out our webpages to use the API to list out all the hats. We would also create a forms page using react that would
submit and create hats.
