import React, { useEffect, useState } from 'react';
function ShoesForm(props) {
    const [model, setModel] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPicture_url] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);

    const handleModelChange = (e) => {
        setModel(e.target.value)
    }

    const handleManufacturerChange = (e) => {
        setManufacturer(e.target.value)
    }

    const handleColorChange = (e) => {
        setColor(e.target.value)
    }


    const handlePicture_urlChange = (e) => {
        setPicture_url(e.target.value)
    }

    const handleBinChange = (e) => {
        setBin(e.target.value)
    }


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/'
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json();
                setBins(data.bins);
        }
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const data = {};
        data.model = model;
        data.manufacturer = manufacturer;
        data.color = color;
        data.picture_url = picture_url;
        data.bin = bin;

        console.log(data);
        const url = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe)
            setModel('');
            setManufacturer('');
            setColor('');
            setPicture_url('');
            setBin('');
        }
    }

    useEffect(() => {
        fetchData();
    }, [])





    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Shoe Submission</h1>
                    <form id="create-shoe-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={handleModelChange} placeholder="Model" value={model} required type="text" id="model" className="form-control" name="model" />
                            <label htmlFor="model">Model</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required name="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} placeholder="Color" value={color} required type="text" id="color" className="form-control" name="color" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description">Picture URL</label>
                            <input onChange={handlePicture_urlChange} required type="text" value={picture_url} id="picture_url" className="form-control" name="picture_url"/>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleBinChange}required id="bin" value={bin} className="form-select" name="bin">
                                <option value="">Choose Bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.id} value={bin.id}>{bin.closet_name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default ShoesForm
