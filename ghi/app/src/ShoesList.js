import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";

function ShoesList() {
    const [shoes,setShoes] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/'

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setShoes(data.shoes)
            }
        } catch(e) {
            console.error(e)
        }
    }

    useEffect(()=> {
        fetchData();
    }, []);

    const remove = async (shoe) => {
        try {
            const deleteId = shoe.id
            const url = `http://localhost:8080/api/shoes/${deleteId}`
            const fetchConfig = {
                method: "delete"
            };
            const response = await fetch(url, fetchConfig);
            if (response.ok) {
                console.log("shoe deleted")
                fetchData()
            }
    }   catch (e) {
            console.log(e)
        }

    };

    return (
        <>
          <NavLink className="nav-link" to="/shoes/new">Add another shoe</NavLink>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Model</th>
                <th>Manufacturer</th>
                <th>Color</th>
                <th>Picture</th>
                <th>Remove a Shoe</th>
              </tr>
            </thead>
            <tbody>
              {shoes.map(shoe => (
                <tr key={shoe.id}>
                  <td>{shoe.model}</td>
                  <td>{shoe.manufacturer}</td>
                  <td>{shoe.color}</td>
                  <td><img width="120px" alt="no image" src={shoe.picture_url}/></td>
                  <td><button onClick={()=> remove(shoe)}>Remove</button></td>
                </tr>
              ))}
            </tbody>
          </table>
        </>
      );
    }



export default ShoesList;
