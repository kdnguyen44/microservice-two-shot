import { NavLink } from "react-router-dom";
import React, { useEffect, useState } from 'react';

function HatsList(props) {
    const [hats, setHats] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/hats';

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setHats(data.hats)
            }
        } catch (e) {
            console.error(e)
        }
    }

    const deleteHat = async(hat) => {
        try{
            const deleteId = hat.id
            const deleteUrl = `http://localhost:8090/api/hats/${deleteId}`
            const fetchConfig = {
                method: "delete",
            };
            const response = await fetch(deleteUrl, fetchConfig);
            if (response.ok) {
                console.log("shoe deleted")
                fetchData()
            }
        } catch (e) {
            console.log(e)
        }


    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
        <section className="py-5 text-center container">
            <div className="row py-lg-5">
                <div className="col-lg-6 col-md-8 mx-auto">
                    <h1 className="fw-light">Hat Collection</h1>
                    <p>
                    <NavLink to="/hats/new" className="btn btn-primary my-2">Add a Hat</NavLink>
                    </p>
                </div>
            </div>
        </section>
        <div className="album py-5 bg-body-tertiary">
            <div className="container">
                <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                    {hats.map(hat => {
                        return (
                            <div className="col" key={hat.id}>
                                <div className="card shadow-sm">
                                    <img className="card-img-top" width="100%" height="100%" src={hat.picture} role="img" />
                                    <div className="card-body">
                                        <p className="card-text"><b>Fabric:</b> {hat.fabric}</p>
                                        <p className="card-text"><b>Color:</b> {hat.color}</p>
                                        <p className="card-text"><b>Closet Location:</b> {hat.location.closet_name}</p>
                                        <div className="d-flex justify-content-between align-items-center">
                                            <div className="btn-group">
                                                <button onClick={() => deleteHat(hat)} type="button" className="btn btn-sm btn-outline-secondary">Delete</button>
                                            </div>
                                            <small className="text-body-secondary">Style: {hat.style_name}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
        </>
    )
}

export default HatsList;
