import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';
import HatsList from './HatsList';
import HatsForm from './HatsForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="hats" element={<HatsList />} />
          <Route path="hats/new" element={<HatsForm />} />
          <Route path="shoes" element={<ShoesList />} />
          <Route path="shoes/new" element={<ShoesForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
